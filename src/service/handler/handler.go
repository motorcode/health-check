package handler

import (
	"golang.org/x/net/context"

	proto "gitlab.com/contetto-source/health-check-srv/src/service/proto"
)

type Activity struct{}

// Create provides add a new feed for the user
func (s *Activity) Get(ctx context.Context, req *proto.EmptyItem, rsp *proto.ResultItem) error {
	rsp.Result = "OK"
	return nil
}
