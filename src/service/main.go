package main

import (
	"log"

	"github.com/micro/go-micro"
	_ "github.com/micro/go-plugins/broker/rabbitmq"
	"gitlab.com/contetto-source/health-check-srv/src/service/handler"
	proto "gitlab.com/contetto-source/health-check-srv/src/service/proto"
	"gitlab.com/contetto-source/health-check-srv/src/service/restful"

	_ "gitlab.com/contetto-source/kuber-plugin"
)

func main() {
	service := micro.NewService(
		micro.Name("health-check-srv"),
	)

	log.Println("initialization of the service")
	service.Init()
	log.Println("initialization of the restful part")
	go restful.Init()
	proto.RegisterHealthCheckHandler(service.Server(), new(handler.Activity))

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
