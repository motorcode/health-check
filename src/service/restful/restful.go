// Package restful provides implementation of restful endpoints
package restful

import (
	//"strings"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

// BrandHandler define basic handler for the endpoints
type BrandHandler struct {
	handler func(http.ResponseWriter, *http.Request)
}

func (th *BrandHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	th.handler(w, r)
}

func Health(w http.ResponseWriter, r *http.Request) {
	log.Println("health check")
	w.WriteHeader(http.StatusOK)
}

// Init provides initialization of restful module
func Init() {
	router := mux.NewRouter()
	router.Handle("/health-check-srv/v1/health", &BrandHandler{handler: Health}).Methods("GET")
	http.ListenAndServe(os.Getenv("MICRO_MUX_ADDRESS"), router)
}
