# Makefile compiles a micro service into a Go executable while fetching the
# dependencies.
# Use "make help" for a guide about using it.
#
# Feel free to mention @onairnetlines on Slack if you have any questions
# regarding to this make file or the make command, or if you want it to change
# the way it works.
#
# Few notes:
# 1. Deploy is not implemented. It just shows "PLACEHOLDER?".
# 2. Clean is AGGRESSIVE. It deletes ALL directories in ./src/ EXCEPT service.
# It does so to clean out all packages downloaded by go get. That's a bit
# dangerous. Feel free to remove it if you prefer.
# 3. make run does NOT build automatically. 
# 4. make lint does NOT get dependencies automatically. It's for performance
# reasons to iterate quickly on development. See below.
#
#
.PHONY: deps_get build lint unit-test-internal unit-test-race-internal unit-test-coverage-internal unit-test unit-test-race unit-test-coverage doc run clean help

ROOTREPO := ${PWD}
GOPATHVENDOR := ${ROOTREPO}/vendor
GOPATHTOOLS := ${ROOTREPO}/.tools
GOPATH := ${GOPATHVENDOR}:${ROOTREPO}:${GOPATH}
PATH := ${ROOTREPO}/vendor/bin:${ROOTREPO}/.tools/bin:${ROOTREPO}/bin:${PATH}

# Base path of the Contetto git repositories inside the GOPATH.
# The rest will be filled by $CI_PROJECT_PATH environment variable at runtime.
#
# It is used so far only in CI context, to disallow downloading another branch
# of itself for the imported packages.
CONTETTO_IN_GOPATH := ${GOPATHVENDOR}/src/gitlab.com

DOCKERPATH := ${ROOTREPO}/vendor/src/github.com/docker/docker

export ROOTREPO
export GOPATH
export PATH

default: unit-test-coverage

# TODO: Fix go get -u. It complains of "is not a known version control source"
# error messagge.
#
# Since the development in a branch does not work because of go get, it creates
# a symbolic link, avoiding go get to try to fetch it from repo. This link
# points to the currently building directory, because it already contain the git
# files at the right commit.
#
# To allow the link to exist, it create parent directories, and then delete the
# last child directory which is going to be the symbolic link. It only remove
# the child directory if the child directory is appropriately empty.
#
# It also removes the swagger update in go get since, now, the build environment
# is providing it as a cache. It only go gets if we're not already in CI.
deps_get:
	if [ ! -d vendor ]; then mkdir vendor; fi
	if [ ! -d .tools ]; then mkdir .tools; fi
	if [ ! -d vendor/src ]; then mkdir vendor/src; fi
	if [ -n "$$CI_SERVER" ]; then \
		mkdir --verbose --parents ${CONTETTO_IN_GOPATH}/$$CI_PROJECT_PATH && \
		rm    --verbose --dir ${CONTETTO_IN_GOPATH}/$$CI_PROJECT_PATH && \
	 	ln    --verbose --symbolic ${PWD} ${CONTETTO_IN_GOPATH}/$$CI_PROJECT_PATH; \
	fi
	cd src/service/ && \
	go get -v ./...
	if [ -z "$$CI_SERVER" ]; then \
		GOPATH=${GOPATHTOOLS} go get -v github.com/go-swagger/go-swagger/cmd/swagger; \
	fi


# WARNING: DUE TO LONG TIME IT TAKEN TO RUN THE LINTER, FOR NOW, IT IS
# DEPRECATED.
#
# IT MAY BE ENABLED BACK IN THE FUTURE THOUGH.
#
# About the deadline, it is simply too short. On my computer, it failed to run
# in a timely 5 seconds just because I had little usage on my hard disk.
# It is set up here at 60 seconds.
#
# Also, the number of concurrent linters by default (16) is just way too heavy.
# Way way too heavy. It brings down my hard disk in a minute.
# So I lowered it to two, that's enough.
#
# Finally, and maybe most importantly, I added a filter to filter protobuf
# generated go files. It was worst than noisy on the output.
lint: deps_get
	echo Deprecated

# Cannot provide an output file for go build when there's multiple packages in
# the repo.
build: lint
	cd src/service/ && \
	GOOS=linux go install -v -ldflags '-w' ./...
	cd src/service/ && \
	swagger generate spec -o ${ROOTREPO}/swagger.json


unit-test-internal:
	cd src/service/ && \
	go test -p 1 $$(go list ./... | grep -v /vendor/)

unit-test: deps_get unit-test-internal

unit-test-race-internal:
	cd src/service/ && \
	go test -race -p 1 $$(go list ./... | grep -v /vendor/)

unit-test-race: deps_get unit-test-race-internal

# Checks the test coverage.
# If over 40%, it's sufficient.
# If it's under, it's not sufficient.
#
# Trailing slashes are needed because make does something like this for each
# line:
# /bin/sh -c "line"
# So conditions wouldn't work.

unit-test-coverage-internal:
	cd src/service/ && \
	go test -covermode=count ./... > ${ROOTREPO}/coverage.out
	@ echo Exited with status: $$?
	@ COVERAGE=$$(cat coverage.out | head -n 1 --silent | awk '{ print $$5 }'); COVERAGE=$${COVERAGE%.*}; cd ${ROOTREPO}; \
	if [ "$$COVERAGE" -ge 40 ]; then \
		echo -e "\n\nMinimum coverage is sufficient: $$COVERAGE%\n\n"; \
	else \
		echo -e "\n\nMinimum coverage is NOT sufficient: $$COVERAGE%\n\n"; \
		exit 33; \
	fi;

unit-test-coverage: deps_get unit-test-coverage-internal

# Builds the image and push it to the registry;
# It uses the ImageName file to know if it should generate (or not) the docker
# image.
# If this file is not empty, it builds the image with the specified image name.
# The ImageName file should NOT have a BOM and should contains the full address
# to a registry.
# This is left as a reference, it has been moved out to the .gitlab-ci.yml file.
#docker-deploy:
#	if [ -s ImageName ]; then \
#	IMAGENAME=$$(cat ImageName) && \
#	docker build -t $$IMAGENAME . && \
#	docker push $$IMAGENAME; \
#	fi;

doc:
	godoc -http=:6060 -index

run:
	./bin/service

clean:
	rm --recursive --dir --force ./bin/*
	rm --recursive --dir --force ./pkg/*
	rm --recursive --dir --force ./.tools
	rm --recursive --dir --force ./vendor

help:
	@ echo -e \
	"This make helps you to test and build microservices. \n \
	If you just cloned the repo, you would type: \n \
	$$ make get_deps \n \
	$$ make \n \
	\n \
	After, you just need to do "make". \n \
	\n \
	There's the following make commands: \n \
	make == make lint + build + unit-test + unit-test-race + unit-test-coverage \n \
	make deps_get: download the deps of the service + the metalinter. \n \
	make build: lint + builds the Go executable into ./bin/service. Prefer to call make except you need to build even with the test erros. \n \
	make unit-test : lint + build + check the basic unit testing. \n \
	make unit-test-race : lint + build + unit-test + check the unit testing with race conditions. \n \
	make unit-test-coverage : lint + build + unit-test + unit-test-race + checks the unit testing coverage of your code. \n \
	make deploy: should deploy your code. Right now, it is not implemented. \n \
	make clean: aggressive cleaning. Cleans bin, pkg, and src except src/service. \n \
	make run: run the compiled service. Note that launching this command does NOT compile. Do make for this. \n \
	make doc: Launch a godoc server about your service on port 6060. \n \
	\n \
	In the end, if you have any questions, you can mention @onairnetlines on Slack. \n \
	"